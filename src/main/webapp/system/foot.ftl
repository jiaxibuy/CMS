	</section>
	<!-- js placed at the end of the document so the pages load faster -->
	<script src="${basePath}/system/js/bootstrap.min.js"></script>
	<script src="${basePath}/system/js/jquery.dcjqaccordion.2.7.js"></script>
	<script src="${basePath}/system/js/jquery.scrollTo.min.js"></script>
	<script src="${basePath}/system/js/jquery.nicescroll.js"></script>
	<script src="${basePath}/system/js/dropzone.js"></script>
	<script src="${basePath}/system/js/respond.min.js"></script>
	<script src="${basePath}/system/js/jquery.form.min.js"></script>
	<script src="${basePath}/system/assets/tinymce/jquery.tinymce.min.js"></script>
	<script src="${basePath}/system/assets/tinymce/tinymce.min.js"></script>
	<script src="${basePath}/system/js/bootstrap-switch.js"></script>
	<script src="${basePath}/system/js/bootbox.min.js"></script>
	<script src="${basePath}/system/js/jquery.json.js"></script>
	<!--common script for all pages-->
	<script src="${basePath}/system/js/common-scripts.js"></script>

</body>
</html>
